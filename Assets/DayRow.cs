﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayRow : MonoBehaviour {

    [SerializeField]
    Text dateLabel;
    [SerializeField]
    Text[] infoLabels;
    [SerializeField]
    Text[] classRoomLabels;

    public void Setup(DateTime date, ClassInfomation[] infomations) {
        dateLabel.text = $"{date.Month} 月 {date.Day} 日";

        for(int i = 0; i < 3; i++) {
            if (infomations[i].isNull) continue;
            switch (UserSetting.fetchType) {
                case FetchType.Teacher:
                    infoLabels[i].text = infomations[i].classCode + " - " + infomations[i].className;
                    break;
                case FetchType.ClassCode:
                    infoLabels[i].text = $"{infomations[i].className} ({infomations[i].teacher})";
                    break;
            }
        }
    }
}
