﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoHideScrollbar : MonoBehaviour {

    [SerializeField]
    Image handler;

    public float targetAlpha = 160f / 255f;
    public float holdDuration = 0.5f;
    public float fadeDuration = 1f;

    float timer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (timer < 0f) {
            enabled = false;
        }
        handler.color = ChangeAlpha(handler.color, Mathf.Lerp(0f, targetAlpha, timer / fadeDuration));
        timer -= Time.deltaTime;
	}

    Color ChangeAlpha(Color color, float alpha) {
        color.a = alpha;
        return color;
    }

    public void OnBarMove() {
        enabled = true;
        timer = holdDuration + fadeDuration;
    }
}
