﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonthPage : MonoBehaviour {

    [SerializeField]
    Text headerLabel;
    [SerializeField]
    DayRow rowTemplate;

    public DateTime date;
    public int index { get; private set; }

	// Use this for initialization
	void Start () {
        headerLabel.text = $"{date.Year} 年 {date.Month} 月";

        int rowCount = (int)(((RectTransform)transform).sizeDelta.y / ((RectTransform)rowTemplate.transform).sizeDelta.y) + 1;
        for (int i = 0; i < rowCount; i++) {

        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
