﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class AndroidToggle : MonoBehaviour, IPointerClickHandler {

    [SerializeField]
    Image background, colorMask;
    [SerializeField]
    RectTransform handler;
    [SerializeField]
    Color _diactiveColor, _activeColor;
    [SerializeField]
    bool _isOn;

    public float animationDuration = 0.5f;
    public Toggle.ToggleEvent onValueChanged = new Toggle.ToggleEvent();
    public bool isOn {
        get {
            return _isOn;
        }
        set {
            SetOn(value, true, true);
        }
    }
    public Color diactiveColor {
        get {
            return background.color;
        }
        set {
            background.color = value;
        }
    }
    public Color activeColor {
        get {
            return colorMask.color;
        }
        set {
            colorMask.color = value;
        }
    }

	// Use this for initialization
	void Start () {
	}

    void SetOn(bool isOn, bool animatly, bool triggerEvent) {
        _isOn = isOn;

        float maskScale = (isOn) ? 1f : 0f;
        float handlerX = (isOn) ? background.rectTransform.sizeDelta.x - handler.sizeDelta.x : 0f;

        if (triggerEvent) {
            onValueChanged.Invoke(isOn);
        }

        if (animatly) {
            colorMask.transform.DOScale(maskScale, animationDuration);
            handler.DOAnchorPosX(handlerX, animationDuration);
        } else {
            colorMask.transform.localScale = Vector3.one * maskScale;
            handler.anchoredPosition = new Vector2(handlerX, handler.anchoredPosition.y);
        }
    }

    [ContextMenu("Refresh")]
    public void Refresh() {
        diactiveColor = _diactiveColor;
        activeColor = _activeColor;
        SetOn(isOn, false, false);
    }

    public void OnPointerClick(PointerEventData eventData) {
        SetOn(!isOn, true, true);
    }
}
