﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SideMenuView : MonoBehaviour {

    [SerializeField]
    new Animation animation;
    public string onAnimation, offAnimation;

    [SerializeField]
    Text keyInputTitleLabel;
    [SerializeField]
    InputField keyInput;
    [SerializeField]
    AndroidToggle toggle;

    public bool userInterfaceActive {
        get {
            return GetComponent<GraphicRaycaster>().enabled;
        }
        set {
            GetComponent<GraphicRaycaster>().enabled = value;
        }
    }

    private void Awake() {
        if (gameObject.activeSelf) { gameObject.SetActive(false); }
    }

    // Use this for initialization
    void Start () {
		
	}

    private void OnEnable() {
        keyInput.text = toggle.isOn ? UserSetting.classCode : UserSetting.teacherName;
    }

    public void SetEnable(bool isOn) {
        if (isOn) {
            gameObject.SetActive(true);
            userInterfaceActive = true;
            animation.Play(onAnimation);
        } else {
            userInterfaceActive = false;
            animation.Play(offAnimation);
        }
    }

    public void AnimationEvent_Hide() {
        gameObject.SetActive(false);
    }

    public void OnToggleValueChanged(bool isOn) {
        UserSetting.fetchType = isOn ? FetchType.ClassCode : FetchType.Teacher;

        keyInputTitleLabel.text = isOn ? "班級代號" : "教師名稱";
        keyInput.text = isOn ? UserSetting.classCode : UserSetting.teacherName;
    }

    public void OnUserNameEndEdit(string value) {
        if (string.IsNullOrWhiteSpace(value)) {
            keyInput.text = toggle.isOn ? UserSetting.classCode : UserSetting.teacherName;
            return;
        }

        if (toggle.isOn) {
            UserSetting.classCode = keyInput.text;
        }
        else {
            UserSetting.teacherName = keyInput.text;
        }
    }
}

public static class UserSetting {
    public struct Key {
        public const string FetchType = "fetchType";
        public const string TeacherName = "teacherName";
        public const string ClassCode = "classCode";
    }

    public static FetchType fetchType {
        get {
            return (FetchType)System.Enum.Parse(typeof(FetchType), PlayerPrefs.GetString(Key.FetchType, FetchType.Teacher.ToString()));
        }
        set {
            PlayerPrefs.SetString(Key.FetchType, value.ToString());
        }
    }

    public static string teacherName {
        get {
            return PlayerPrefs.GetString(Key.TeacherName, "WixLitariz");
        }
        set {
            PlayerPrefs.SetString(Key.TeacherName, value);
        }
    }

    public static string classCode {
        get {
            return PlayerPrefs.GetString(Key.ClassCode, "CU101");
        }
        set {
            PlayerPrefs.SetString(Key.ClassCode, value);
        }
    }
}